import {
	SET_TEST,
} from './actionTypes.js';

export default (state = [], action) => {
	switch (action.type) {
        case SET_TEST: {
			return Object.assign({}, state, {
				test: action.test
			});
		}
		default:
			return state
	}
};