import React, { Component } from 'react';
import {connect} from 'react-redux';
import {config} from '../config.js';

import { setTest } from '../actions.js';

const mapStateToProps = (state) => ({
  test: state.test,
});

const mapDispatchToProps = (dispatch) => ({
  setTest(test) {
    dispatch(setTest(test));
  },
});

class TestPage extends Component {
  render() {
    return (
      <div></div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestPage);




