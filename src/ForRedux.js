import React, { Component } from 'react';
import {connect} from 'react-redux';

import { setTest } from './actions.js';

import TestPage from './pages/TestPage.js';

const mapStateToProps = (state) => ({
  test: state.test,
});
const mapDispatchToProps = (dispatch) => ({
  setTest(test) {
    dispatch(setTest(test));
  },
});

class ForRedux extends Component {
  render() {
    return (
        <div>
          {/*pages*/}
        </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForRedux);




