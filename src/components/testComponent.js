import React, { Component } from 'react';
import {connect} from 'react-redux';

import { setTest } from '../actions.js';


const mapStateToProps = (state) => ({
  test: state.test,
});
const mapDispatchToProps = (dispatch) => ({
  setTest(test) {
    dispatch(setTest(test));
  },
});

class TestComponent extends Component {
  render() {
    return (
      <div></div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestComponent);




