import {
	SET_TEST,
} from './actionTypes.js';


export const setTest = (test) => ({type: SET_TEST, test});