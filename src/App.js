import React, { Component } from 'react';
import './css/App.css';

import {Provider} from 'react-redux';
import {createStore} from 'redux';

import reducers from './reducers.js';
import ForRedux from './ForRedux.js';

class App extends Component {
  constructor(){
    super();
    this.initialState = {
      test: 0,
    };
    this.store = createStore(reducers, this.initialState);
  }
  render() {
    return (
      <Provider store={this.store}>
        <ForRedux />
      </Provider>
    );
  }
}

export default App;
